package com.ii2.itac.utilities;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class CommonUtilsTest {
	
	@Autowired
	CommonUtils obj;

	@Test
	void getActiveOrgsForUser() {	
		Long userId= 1L;
		assertThat(obj.getActiveOrgsForUser(userId).size() == 2, is(true));
	}

}
