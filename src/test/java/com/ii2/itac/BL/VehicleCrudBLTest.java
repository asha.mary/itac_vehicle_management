package com.ii2.itac.BL;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.ii2.itac.DTO.VehicleDetailsDTO;
import com.ii2.itac.exceptions.BadRequestAlertException;
import com.ii2.itac.exceptions.NoActiveOrganizationException;
import com.itac.utilities.LoggerStackTraceUtil;

@SpringBootTest
class VehicleCrudBLTest {

	@Autowired
	VehicleCrudBL blObj;

	@Autowired
	VehicleDetailsDTO vehicle;

	/**
	 * function to fetch vehicle details of user
	 */
	@Test
	void getVehicleListForUserTest() {
		Long userId = 1L;
		try {
			assertThat(blObj.getVehicleListForUser(userId).size() == 4, is(true));
		} catch (Exception e) {
			LoggerStackTraceUtil.printErrorMessage(e);
		}
	}

	/**
	 * list vehicles for inactive org
	 */
	@Test
	void getVehicleListNoActiveOrgTest() {
		Long userId = 2L;
		Boolean check = false;
		try {
			blObj.getVehicleListForUser(userId);
		} catch (NoActiveOrganizationException th) {
			check = true;
			assertThat(check, is(true));
		} catch (Exception e) {
			LoggerStackTraceUtil.printErrorMessage(e);
		}
	}

	/**
	 * crete vehicle with an object containing an id
	 */
	@Test
	void createVehicleWithIdTest() {
		vehicle.setVehicleId(5L);
		vehicle.setVehicleName("vehicle 5");
		vehicle.setLicensePlate("KL-07-1675");
		vehicle.setVehicleTypeId(1);
		vehicle.setOrgId(1L);
		Boolean check = false;
		try {
			blObj.createVehicle(vehicle);
		} catch (BadRequestAlertException th) {
			check = true;
			assertThat(check, is(true));
		} catch (Exception e) {
			LoggerStackTraceUtil.printErrorMessage(e);
		}
	}

	/**
	 * function to create vehicle
	 */
	//@Test
	void createVehicleTest() {
		vehicle.setVehicleName("vehicle 6");
		vehicle.setLicensePlate("KL-08-1175");
		vehicle.setVehicleTypeId(1);
		vehicle.setOrgId(1L);
		try {
			assertThat(blObj.createVehicle(vehicle) != null, is(true));
		} catch (Exception e) {
			LoggerStackTraceUtil.printErrorMessage(e);
		}
	}

	/**
	 * function to update vehicle
	 */
	//@Test
	void updateVehicleTest() {
		vehicle.setVehicleId(5L);
		vehicle.setVehicleName("vehicle 6");
		vehicle.setLicensePlate("KL-07-1675");
		vehicle.setVehicleTypeId(1);
		vehicle.setOrgId(1L);
		try {
			assertThat(blObj.updateVehicle(vehicle) != null, is(true));
		} catch (Exception e) {
			LoggerStackTraceUtil.printErrorMessage(e);
		}
	}

	/**
	 * function to delete vehicle
	 */
	//@Test
	void deleteVehicleTest() {
		Long userId = 1L;
		Long vehicleId = 5L;
		try {
			blObj.deleteVehicle(vehicleId);
			assertThat(blObj.getVehicleListForUser(userId).size() == 4, is(true));
		} catch (Exception e) {
			LoggerStackTraceUtil.printErrorMessage(e);
		}
	}

}
