package com.ii2.itac.BL;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class VehicleDetailsBLTest {

	@Autowired
	VehicleDetailsBL blObj;
	

	/**
	 * test case for fetching vehicle subtypes 
	 */
	//@Test
	void getVehicleSubtypesTest() {
		Integer vehicleTypeId = 1;
		assertThat(blObj.getVehicleSubtypes(vehicleTypeId).size() == 2, is(true));
	}
	
	
	/**
	 * test case for fetching devices of a user
	 */
	@Test
	void getDevicesForUserTest() {
		Long userId = 1L;
		assertThat(blObj.getDevicesForUser(userId).size() == 6, is(true));
	}

}
