package com.ii2.itac.BL;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class CurrentLocationBLTest {
	
	@Autowired
	CurrentLocationBL blObj;

	@Test
	void getVehicleListForUserTest() {
		Long vehicleId = 2L;
		assertThat(blObj.getCurrentLocation(vehicleId) != null, is(true));
	}

}
