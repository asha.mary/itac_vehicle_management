package com.ii2.itac.exception.handler;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.ii2.itac.exceptions.BadRequestAlertException;
import com.ii2.itac.exceptions.DataNotFoundException;
import com.ii2.itac.exceptions.NoActiveOrganizationException;
import com.ii2.itac.exceptions.StorageFileNotFoundException;


@ControllerAdvice
public class RestResponseExceptionHandler extends ResponseEntityExceptionHandler{
	
		
	@ExceptionHandler(NoActiveOrganizationException.class)
    protected ResponseEntity<String> handleNoActiveOrgException(NoActiveOrganizationException ex){

      return ResponseEntity
              .status(HttpStatus.BAD_REQUEST)
              .body(ex.getMessage());
    }
	
	@ExceptionHandler(BadRequestAlertException.class)
    protected ResponseEntity<String> handleBadRequestException(BadRequestAlertException ex){

      return ResponseEntity
              .status(HttpStatus.BAD_REQUEST)
              .body(ex.getMessage());
    }

	@ExceptionHandler(DataNotFoundException.class)
    protected ResponseEntity<String> handleDataNotFoundException(DataNotFoundException ex){

      return ResponseEntity
              .status(HttpStatus.INTERNAL_SERVER_ERROR)
              .body(ex.getMessage());
    }
	
	@Override
	   protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
	                 HttpHeaders headers, HttpStatus status, WebRequest request) {
	          List<String> validationList = ex.getBindingResult().getFieldErrors().stream().map(fieldError->fieldError.getDefaultMessage()).collect(Collectors.toList());
	          return new ResponseEntity<>(validationList, status);
	   }

	@ExceptionHandler(StorageFileNotFoundException.class)
	public ResponseEntity<?> handleStorageFileNotFound(StorageFileNotFoundException exc) {
		return ResponseEntity.notFound().build();
	}
}
