package com.ii2.itac.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ii2.itac.BL.CurrentLocationBL;
import com.ii2.itac.DTO.CurrentLocationDTO;

@Service
public class CurrentLocationService {
	
	@Autowired
	CurrentLocationBL currentLocationBL;
	
	/**
	 * 
	 * @param vehicleId
	 * @return
	 */
	public CurrentLocationDTO getCurrentLocation(Long vehicleId) {
		return currentLocationBL.getCurrentLocation(vehicleId);
	}

}
