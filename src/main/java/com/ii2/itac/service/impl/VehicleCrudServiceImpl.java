package com.ii2.itac.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ii2.itac.BL.VehicleCrudBL;
import com.ii2.itac.DTO.VehicleDetailsDTO;
import com.ii2.itac.exceptions.BadRequestAlertException;
import com.ii2.itac.exceptions.DataNotFoundException;
import com.ii2.itac.exceptions.NoActiveOrganizationException;
import com.ii2.itac.service.VehicleCrudService;

@Component
public class VehicleCrudServiceImpl implements VehicleCrudService {
	
	@Autowired
	VehicleCrudBL vehicleCrudBL;

	@Override
	public List<VehicleDetailsDTO> getVehicleListForUser(Long userId) throws NoActiveOrganizationException ,DataNotFoundException {
		return vehicleCrudBL.getVehicleListForUser(userId);
	}

	@Override
	public VehicleDetailsDTO createVehicle(VehicleDetailsDTO vehicle) throws BadRequestAlertException{
		return vehicleCrudBL.createVehicle(vehicle);
	}

	@Override
	public VehicleDetailsDTO updateVehicle(VehicleDetailsDTO vehicle) throws BadRequestAlertException{
		return vehicleCrudBL.updateVehicle(vehicle);
	}

	@Override
	public void deleteVehicle(Long vehicleId) throws BadRequestAlertException{
		vehicleCrudBL.deleteVehicle(vehicleId);
	}

}
