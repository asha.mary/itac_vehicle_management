package com.ii2.itac.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.ii2.itac.DTO.VehicleDetailsDTO;
import com.ii2.itac.exceptions.BadRequestAlertException;
import com.ii2.itac.exceptions.DataNotFoundException;
import com.ii2.itac.exceptions.NoActiveOrganizationException;

@Service
public interface VehicleCrudService {
	
	List<VehicleDetailsDTO> getVehicleListForUser(Long userId) throws NoActiveOrganizationException,DataNotFoundException;
	
	VehicleDetailsDTO createVehicle(VehicleDetailsDTO vehicle) throws BadRequestAlertException;
	
	VehicleDetailsDTO updateVehicle(VehicleDetailsDTO vehicle) throws BadRequestAlertException;
	
	void  deleteVehicle(Long vehicleId) throws BadRequestAlertException;

}
