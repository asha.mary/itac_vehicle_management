package com.ii2.itac.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ii2.itac.BL.VehicleDetailsBL;
import com.ii2.itac.DTO.DeviceDetailsDTO;
import com.ii2.itac.entity.Department;
import com.ii2.itac.entity.Tags;
import com.ii2.itac.entity.VehicleSubtype;
import com.ii2.itac.entity.VehicleType;
import com.ii2.itac.repository.TagsRepository;
import com.ii2.itac.repository.VehicleTypeRepository;

@Service
public class VehicleDetailsService {
	
	@Autowired
	VehicleDetailsBL vehicleDetailsBL;
	
	@Autowired
	VehicleTypeRepository vehicleTypeRepo;
	
	@Autowired
	TagsRepository tagsRepo;	
	
	/**
	 * 
	 * @return
	 */
	public List<VehicleType> getVehicleTypes() {
		return vehicleTypeRepo.findAll();
	}
	
	/**
	 * 
	 * @param vehicleTypeId
	 * @return
	 */
	public List<VehicleSubtype> getVehicleSubtypes(Integer vehicleTypeId){		
		return vehicleDetailsBL.getVehicleSubtypes(vehicleTypeId);
	}
	
	/**
	 * 
	 * @return
	 */
	public List<Tags> getTags() {
		return tagsRepo.findAll();
	}
	
	
	/**
	 * 
	 * @param userId
	 * @return
	 */
	public List<DeviceDetailsDTO> getDevicesForUser(Long userId) {
		return vehicleDetailsBL.getDevicesForUser(userId);
	}
	
	/**
	 * 
	 * @param userId
	 * @return
	 */
	public List<Department> getDepartments(Long userId) {
		return vehicleDetailsBL.getDepartments(userId);
	}
}
