package com.ii2.itac.utilities;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ii2.itac.entity.Organization;
import com.ii2.itac.entity.UsersOrgs;
import com.ii2.itac.repository.OrganizationRepository;
import com.ii2.itac.repository.UsersOrgsRepository;
import com.itac.utilities.LoggerStackTraceUtil;

@Component
public class CommonUtils {

	@Autowired
	UsersOrgsRepository usersOrgsRepo;

	@Autowired
	OrganizationRepository orgRepo;

	/**
	 * function user to fetch active organization of a user
	 * 
	 * @param userId
	 * @return
	 */
	public List<Long> getActiveOrgsForUser(Long userId) {

		List<Long> activeOrgs = new ArrayList<>();
		Organization organization = new Organization();
		List<UsersOrgs> usersOrgs = usersOrgsRepo.findByuserId(userId);
		for (UsersOrgs userOrg : usersOrgs) {
			organization = orgRepo.findById(userOrg.getOrgId()).get();
			if (organization.getStatus().equalsIgnoreCase("Active")) {
				activeOrgs.add(organization.getOrgId());
			}

		}
		return activeOrgs;
	}

	/**
	 * function used to find whether vehicle is online or offline or partial
	 * 
	 * @param packetDate
	 * @return
	 */
	public String getVehicleStatus(long packetDate) {
		Calendar cal = Calendar.getInstance();
		String status = "";

		try {
			int timeDiff = (int) ((cal.getTimeInMillis() - packetDate) / (60 * 1000));
			if (timeDiff <= 30) {
				status = "online";
			} else if (timeDiff > 30 && timeDiff <= 360) {
				status = "partial";
			} else {
				status = "offline";
			}
		} catch (Exception e) {
			LoggerStackTraceUtil.printErrorMessage(e);
		}
		return status;

	}
}
