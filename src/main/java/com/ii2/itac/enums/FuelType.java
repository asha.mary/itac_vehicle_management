package com.ii2.itac.enums;

public enum FuelType {
	
	PETROL, DIESEL, GAS
}
