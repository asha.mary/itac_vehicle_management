package com.ii2.itac.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "device_details")
@Getter
@Setter
public class DeviceDetails {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "device_id")
	private Long deviceId;
	
	@Column(name = "device_name", nullable = false)
	private String deviceName;
	
	@Column(name = "device_status")
	private String deviceStatus;
	
	@Column(name = "last_reported_time")
	private Date lastReportedTime;	
	
	@Column(name = "uid")
	private String uid;
	
	@Column(name = "org_id")
	private Long orgId;	
	
	@Column(name = "is_deleted", columnDefinition="tinyint(1) default 0")
	private Boolean isDeleted = false;	
	
	@Column(name = "department_id")
	private Integer departmentId;
	
	@Column(name = "settings_id")
	private Long settingsId;	
	
}
