package com.ii2.itac.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "vehicle_subtype")
@Getter
@Setter
public class VehicleSubtype {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "vehicle_subtype_id")
	private Integer vehicleSubtypeId;	
	
	@Column(name = "name", nullable = false)
	private String name;
	
	@Column(name = "vehicle_type_id", nullable = false)
	private Integer vehicleTypeId;

}
