package com.ii2.itac.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "depot_details")
@Getter
@Setter
public class DepotDetails {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "depot_id")
	private Integer depotId;
	
	@Column(name = "organization", nullable = false)
	private String organization;
	
	@Column(name = "mechanic")
	private String mechanic;
	
	@Column(name = "dispatcher")
	private String dispatcher;
	
	@Column(name = "address")
	private String address;
	
	@Column(name = "is_deleted", columnDefinition="tinyint(1) default 0")
	private Boolean isDeleted = false;
	
	@Column(name = "org_id")
	private Long orgId;
}
