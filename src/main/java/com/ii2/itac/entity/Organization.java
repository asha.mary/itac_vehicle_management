package com.ii2.itac.entity;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "organization")
@Getter
@Setter
public class Organization {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "org_id")
	private Long orgId;
	
	@Column(name = "org_name", nullable = false)
	private String orgName;
	
	@Column(name = "status")
	private String status;
	
	@ManyToMany
	@JoinTable(name = "org_department_mapping", joinColumns = @JoinColumn(name = "org_id"), inverseJoinColumns = @JoinColumn(name = "department_id"))
	Set<Department> departments;
}
