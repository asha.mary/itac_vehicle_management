package com.ii2.itac.entity;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.ii2.itac.enums.FuelType;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "vehicle_details")
@Getter
@Setter
public class VehicleDetails {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "vehicle_id")
	private Long vehicleId;

	@Column(name = "vehicle_name", nullable = false)
	private String vehicleName;

	@Column(name = "license_plate", nullable = false)
	private String licensePlate;

	@Column(name = "vin")
	private String VIN;

	@Column(name = "device_id")
	private Long deviceId;

	@Column(name = "org_id", nullable = false)
	private Long orgId;

	@Column(name = "color")
	private String color;

	@Column(name = "model")
	private String model;

	@Column(name = "additional_info")
	private String additionalInfo;

	@Column(name = "payload_length")
	private Double payloadLength;

	@Column(name = "width")
	private Double width;

	@Column(name = "height")
	private Double height;

	@Column(name = "cargo_capacity")
	private Double cargoCapacity;

	@Column(name = "gross_weight")
	private Double grossWeight;

	@Column(name = "passenger_capacity")
	private Integer passengerCapacity;

	@Column(name = "axle_configuration_total")
	private Double axleConfigurationTotal;

	@Column(name = "drive")
	private Double drive;

	@Column(name = "tire_size")
	private Double tireSize;

	@Column(name = "tire_number")
	private Integer tireNumber;

	@Column(name = "permitted_speed")
	private Double permittedSpeed;

	@Column(name = "chassis_number")
	private String chassisNumber;

	@Column(name = "trailer")
	private String trailer;

	@Column(name = "manufacture_year")
	private Integer manufactureYear;

	@Column(name = "frame_number")
	private String frameNumber;

	@Enumerated(EnumType.STRING)
	@Column(name = "fuel_type")
	private FuelType fuelType;

	@Column(name = "fuel_grade")
	private String fuelGrade;

	@Column(name = "cost_per_litre")
	private Double costPerLitre;

	@Column(name = "tank_capacity")
	private Double tankCapacity;

	@Column(name = "fuel_consumption_per_100km")
	private Double fuelConsumptionPer100km;

	@Lob
	@Column(name = "img")
	private String img;

	@Column(name = "is_deleted", columnDefinition = "tinyint(1) default 0", nullable = false)
	private Boolean isDeleted = false;

	@Column(name = "vehicle_type_id", nullable = false)
	private Integer vehicleTypeId;

	@Column(name = "vehicle_subtype_id")
	private Integer vehicleSubtypeId;

	@Column(name = "depot_id")
	private Integer depotId;
	
	@ManyToMany
	@JoinTable(name = "vehicle_tag_mapping", joinColumns = @JoinColumn(name = "vehicle_id"), inverseJoinColumns = @JoinColumn(name = "tag_id"))
	Set<Tags> vehicleTags;

}
