package com.ii2.itac.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "vehicle_insurance")
@Getter
@Setter
public class VehicleInsurance {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "insurance_id")
	private Long insuranceId;
	
	@Column(name = "vehicle_id", nullable = false)
	private Long vehicleId;

	@Column(name = "insurance_name", nullable = false)
	private String insuranceName;
	
	@Column(name = "insurance_expiry")
	private Date insuranceExpiry;
	
	

}
