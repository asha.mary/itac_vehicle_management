package com.ii2.itac.entity;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name="vehicle_info")
@Getter
@Setter
public class VehicleInfo {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)	
	@Column (name = "id")
	private Long id;
	
	@Column (name = "vehicle_id" ,insertable=false, updatable=false)
	private Long vehicleId;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "vehicle_id")
	private VehicleDetails vehicle;
	
	@Column (name = "trip_id")
	private Integer tripId;
	
	private Double lattitude;
	
	private Double longitude;
	
	private Double speed;
	
	private Double heading;
	
	@Column (name = "battery_status")
	private String batteryStatus;
	
	@Column (name = "device_id")
	private Long deviceId;
	
	@Column (name = "creation_time")
	private Date creationTime;
	
	@Column (name = "org_id")
	private Long orgId;
	
	@Column (name = "event_code")
	private Integer eventCode;
	
	@Column (name = "reset_distance")
	private Double resetDistance;
	
	private Double distance;
	
	private Integer counter;
	
	@Column (name = "issmssent")
	private Boolean isSMSSent;
	
	@Column (name = "trip_type")
	private String tripType;
	
	@Column (name = "trip_status")
	private String tripStatus;
	
	@Column (name = "tank_capacity")
	private Double tankCapacity;
	
	@Column (name = "stable_fuel_val")
	private Double stableFuelVal;
	
	@Column (name = "last_moved_time")
	private Date lastMovedTime;
	
	@Column (name = "fence_id")
	private Integer fenceId;
	
	@Column (name = "in_fence_time")
	private String inFenceTime;
	
	@Column (name = "esn_number")
	private String esnNumber;
	
	@Column (name = "offline_flag")
	private Boolean offlineFlag;
	
	@Column (name = "current_status")
	private Integer currentStatus;
	
	private String reason;
	
	@Column (name = "driver_name")
	private String driverName;
	
	@Column (name = "mob_no")
	private String mobNo;
	
	@Column (name = "fence_name")
	private String fenceName;
	
	@Column (name = "vehicle_status")
	private String vehicleStatus;

}
