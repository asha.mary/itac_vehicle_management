package com.ii2.itac.DTO;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DeviceDetailsDTO {
	
	Long deviceId;
	
	String deviceName;	
	
	String departMentName;

}
