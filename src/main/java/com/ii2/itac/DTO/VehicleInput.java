package com.ii2.itac.DTO;

import java.util.Set;

import javax.validation.constraints.NotNull;

import com.ii2.itac.entity.Tags;
import com.ii2.itac.enums.FuelType;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class VehicleInput {

	Long vehicleId;

	@NotNull(message = "Vehicle Name cannot be empty.")
	String vehicleName;

	@NotNull(message = "License Plate cannot be empty.")
	String licensePlate;

	String VIN;

	Long deviceId;

	@NotNull(message = "Organization cannot be empty.")
	Long orgId;

	String color;

	String model;

	String additionalInfo;

	Double payloadLength;

	Double width;

	Double height;

	Double cargoCapacity;

	Double grossWeight;

	Integer passengerCapacity;

	Double axleConfigurationTotal;

	Double drive;

	Double tireSize;

	Integer tireNumber;

	Double permittedSpeed;

	String chassisNumber;

	String trailer;

	Integer manufactureYear;

	String frameNumber;

	FuelType fuelType;

	String fuelGrade;

	Double costPerLitre;

	Double tankCapacity;

	Double fuelConsumptionPer100km;

	byte[] img;

	Boolean isDeleted =false;

	@NotNull(message = "Vehicle Type cannot be empty.")
	Integer vehicleTypeId;

	Integer vehicleSubtypeId;

	Integer depotId;

	Set<Tags> vehicleTags;

}
