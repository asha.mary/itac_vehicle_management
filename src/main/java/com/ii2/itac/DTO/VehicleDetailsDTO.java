package com.ii2.itac.DTO;

import java.util.List;

import javax.validation.constraints.NotNull;

import org.springframework.stereotype.Component;

import com.ii2.itac.entity.Tags;
import com.ii2.itac.enums.FuelType;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Component
public class VehicleDetailsDTO {

	Long vehicleId;

	@NotNull(message = "Vehicle Name cannot be empty.")
	String vehicleName;

	@NotNull(message = "License Plate cannot be empty.")
	String licensePlate;

	String model;
	
	@NotNull(message = "Vehicle Type cannot be empty.")
	Integer vehicleTypeId;

	String vehicleTypeName;
	
	Integer depotId;

	String depotName;

	String VIN;
	
	Long deviceId;

	String deviceName;

	FuelType fuelType;

	String fuelGrade;

	Double fuelConsumptionPer100km;

	Double tankCapacity;

	Double cargoCapacity;

	String chassisNumber;
	
	Integer vehicleSubtypeId;

	String vehicleSubtypeName;

	Double axleConfigurationTotal;

	Double drive;

	Integer tireNumber;

	Double tireSize;

	Double permittedSpeed;

	Integer passengerCapacity;
	
	List<Tags> tags;	
	
	@NotNull(message = "Organization cannot be empty.")
	Long orgId;
	
	String img;
	
	List<InsuranceDTO> insurances;

}
