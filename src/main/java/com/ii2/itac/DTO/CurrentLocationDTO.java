package com.ii2.itac.DTO;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CurrentLocationDTO {

	Integer vehicleId;

	String vehicleName;

	String licensePlate;

	String vehicleStatus;

	Double fuel;

	String driverName;

	String phoneNumber;

	String department;
	
	String deviceStatus;

	String deviceName;

	Double lattitude;

	Double longitude;

	Double heading;
	
	byte[] img;
}
