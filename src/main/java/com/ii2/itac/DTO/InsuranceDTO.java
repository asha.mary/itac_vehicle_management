package com.ii2.itac.DTO;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class InsuranceDTO {
	
	Long insuranceId;
	
	Long vehicleId;
	
	String insuranceName;
	
	Date insuranceExpiry;

}
