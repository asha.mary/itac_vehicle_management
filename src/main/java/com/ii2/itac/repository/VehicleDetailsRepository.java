package com.ii2.itac.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ii2.itac.entity.VehicleDetails;

@Repository
public interface VehicleDetailsRepository extends JpaRepository<VehicleDetails, Long>{
	
	@Transactional
	@Query("FROM VehicleDetails where orgId in :orgIds and isDeleted = false")
	List<VehicleDetails> findByOrgs(@Param("orgIds") List<Long> orgIds);
	
	@Transactional
	@Query("FROM VehicleDetails where deviceId =:deviceId")
	VehicleDetails findByDeviceId(@Param("deviceId") Long deviceId);
	
	@Transactional
	@Query("FROM VehicleDetails where vehicleId =:vehicleId and isDeleted = false")
	VehicleDetails findByVehicleId(@Param("vehicleId") Long vehicleId);
	
	
	@Modifying
	@Query("UPDATE VehicleDetails SET isDeleted = true, deviceId = NULL where vehicleId =:vehicleId")
	void deleteByVehicleId(@Param("vehicleId") Long vehicleId);
}
