package com.ii2.itac.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ii2.itac.entity.VehicleSubtype;

@Repository
public interface VehicleSubtypeRepository extends JpaRepository<VehicleSubtype, Integer>{

	@Query("FROM VehicleSubtype where vehicleTypeId =:vehicleTypeId")
	List<VehicleSubtype> findByVehicleTypeId(@Param("vehicleTypeId") Integer vehicleTypeId);
}
