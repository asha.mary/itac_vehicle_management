package com.ii2.itac.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ii2.itac.entity.Department;

@Repository
public interface DepartmentRepository extends JpaRepository<Department, Integer>{

}
