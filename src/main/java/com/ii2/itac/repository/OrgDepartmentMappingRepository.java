package com.ii2.itac.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ii2.itac.entity.OrgDepartmentMapping;

@Repository
public interface OrgDepartmentMappingRepository extends JpaRepository<OrgDepartmentMapping, Integer>{
	
	@Query("SELECT departmentId FROM OrgDepartmentMapping where orgId in :orgIds")
	List<Integer> findByOrgIds(@Param("orgIds") List<Long> orgIds);
}
