package com.ii2.itac.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ii2.itac.entity.VehicleInsurance;

@Repository
public interface VehicleInsuranceRepository extends JpaRepository<VehicleInsurance, Long>{

	@Query("FROM VehicleInsurance where vehicleId =:vehicleId")
	List<VehicleInsurance> findByVehicleId(@Param("vehicleId") Long vehicleId);
}
