package com.ii2.itac.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ii2.itac.entity.VehicleInfo;

@Repository
public interface VehicleInfoRepository extends JpaRepository<VehicleInfo, Long>{
	@Query("FROM VehicleInfo where vehicleId =:vehicleId")
	VehicleInfo findByVehicleId(@Param("vehicleId") Long vehicleId);
	
}
