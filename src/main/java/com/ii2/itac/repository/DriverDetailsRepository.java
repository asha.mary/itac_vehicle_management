package com.ii2.itac.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ii2.itac.entity.DriverDetails;

@Repository
public interface DriverDetailsRepository extends JpaRepository<DriverDetails, Long>{

	@Query("FROM DriverDetails where deviceId =:deviceId")
	DriverDetails findByDeviceId(@Param("deviceId") Long deviceId);
}
