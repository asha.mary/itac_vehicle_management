package com.ii2.itac.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ii2.itac.entity.VehicleTagMapping;

@Repository
public interface VehicleTagMappingRepository extends JpaRepository<VehicleTagMapping, Long>{
	
	@Transactional
	@Query("SELECT tagId FROM VehicleTagMapping where vehicleId =:vehicleId")
	List<Integer> findByVehicleId(@Param("vehicleId") Long vehicleId);

}
