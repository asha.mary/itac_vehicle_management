package com.ii2.itac.exceptions;

public class BadRequestAlertException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public BadRequestAlertException(String message)
	  {
	    super(message);
	  }

}
