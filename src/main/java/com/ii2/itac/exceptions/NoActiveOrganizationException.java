package com.ii2.itac.exceptions;

public class NoActiveOrganizationException extends Exception{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public NoActiveOrganizationException(String message)
	  {
	    super(message);
	  }

}
