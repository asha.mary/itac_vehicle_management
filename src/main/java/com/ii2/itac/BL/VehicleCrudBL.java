package com.ii2.itac.BL;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ii2.itac.DTO.InsuranceDTO;
import com.ii2.itac.DTO.VehicleDetailsDTO;
import com.ii2.itac.entity.DepotDetails;
import com.ii2.itac.entity.DeviceDetails;
import com.ii2.itac.entity.Tags;
import com.ii2.itac.entity.VehicleDetails;
import com.ii2.itac.entity.VehicleInsurance;
import com.ii2.itac.entity.VehicleSubtype;
import com.ii2.itac.entity.VehicleType;
import com.ii2.itac.exceptions.BadRequestAlertException;
import com.ii2.itac.exceptions.DataNotFoundException;
import com.ii2.itac.exceptions.NoActiveOrganizationException;
import com.ii2.itac.repository.DepotDetailsRepository;
import com.ii2.itac.repository.DeviceDetailsRepository;
import com.ii2.itac.repository.OrganizationRepository;
import com.ii2.itac.repository.TagsRepository;
import com.ii2.itac.repository.UsersOrgsRepository;
import com.ii2.itac.repository.VehicleDetailsRepository;
import com.ii2.itac.repository.VehicleInsuranceRepository;
import com.ii2.itac.repository.VehicleSubtypeRepository;
import com.ii2.itac.repository.VehicleTagMappingRepository;
import com.ii2.itac.repository.VehicleTypeRepository;
import com.ii2.itac.utilities.CommonUtils;
import com.itac.utilities.LoggerStackTraceUtil;

@Component
public class VehicleCrudBL {

	@Autowired
	UsersOrgsRepository usersOrgsRepo;

	@Autowired
	OrganizationRepository orgRepo;

	@Autowired
	TagsRepository tagRepo;

	@Autowired
	VehicleDetailsRepository vehDetailsRepo;

	@Autowired
	DeviceDetailsRepository deviceDetailsRepo;

	@Autowired
	VehicleTagMappingRepository vehTagMappingRepo;

	@Autowired
	VehicleTypeRepository vehicleTypeRepo;

	@Autowired
	VehicleSubtypeRepository vehicleSubtypRepo;
	
	@Autowired
	VehicleInsuranceRepository insuranceRepo;

	@Autowired
	DepotDetailsRepository depotDetailsRepo;

	@Autowired
	CommonUtils commonUtils;

	ModelMapper mapper = new ModelMapper();

	/**
	 * 
	 * @param userId
	 * @return
	 * @throws NoActiveOrganizationException
	 * @throws DataNotFoundException
	 */
	public List<VehicleDetailsDTO> getVehicleListForUser(Long userId)
			throws NoActiveOrganizationException, DataNotFoundException {
		List<VehicleDetailsDTO> vehicleList = null;
		try {
			List<Long> activeOrgs = commonUtils.getActiveOrgsForUser(userId);
			if (activeOrgs.isEmpty()) {
				throw new NoActiveOrganizationException("User does not have any active organization");
			}
			vehicleList = getVehicleListForOrgs(activeOrgs);
			if (vehicleList.isEmpty()) {
				throw new DataNotFoundException("No data found");
			}

		} catch (Exception e) {
			LoggerStackTraceUtil.printErrorMessage(e);
			throw e;
		}

		return vehicleList;
	}
    /**
     * 
     * @param input
     * @return
     * @throws BadRequestAlertException
     */
	public VehicleDetailsDTO createVehicle(VehicleDetailsDTO input) throws BadRequestAlertException {
		VehicleDetailsDTO result;
		VehicleDetails vehicleDetails;
		VehicleDetails vehInput;
		Long deviceId = input.getDeviceId();
		try {
			if (input.getVehicleId() != null) {
				throw new BadRequestAlertException("A new vehicle cannot already have an ID");
			} else {
				if (deviceId != null) {
					VehicleDetails vehicle = vehDetailsRepo.findByDeviceId(deviceId);
					if (vehicle != null) {
						throw new BadRequestAlertException("Device alreday assigned to another vehicle");
					}
				}
				vehInput = mapper.map(input, VehicleDetails.class);
				vehicleDetails = vehDetailsRepo.save(vehInput);
				List<InsuranceDTO> insuranceList = input.getInsurances();				
				if(insuranceList != null) {
					saveInsuranceDetails(insuranceList, vehicleDetails.getVehicleId());					
				}
				result = getVehicleDetailsDTO(vehicleDetails);
			}
		} catch (Exception e) {
			LoggerStackTraceUtil.printErrorMessage(e);
			throw e;
		}
		return result;
	}

	/**
	 * 
	 * @param input
	 * @return
	 * @throws BadRequestAlertException
	 */
	public VehicleDetailsDTO updateVehicle(VehicleDetailsDTO input) throws BadRequestAlertException {
		VehicleDetailsDTO result;
		VehicleDetails vehicleDetails;
		VehicleDetails vehInput;
		Long deviceId = input.getDeviceId();
		Long vehicleId = input.getVehicleId();
		try {
			if (vehicleId == null) {
				throw new BadRequestAlertException("Invalid ID");
			} else {
				if (deviceId != null) {
					VehicleDetails vehicle = vehDetailsRepo.findByDeviceId(deviceId);
					if (vehicle != null) {
						throw new BadRequestAlertException("Device alreday assigned to another vehicle");
					}
				}
				vehInput = mapper.map(input, VehicleDetails.class);
				vehicleDetails = vehDetailsRepo.save(vehInput);
				List<InsuranceDTO> insuranceList = input.getInsurances();				
				if(insuranceList != null) {
					saveInsuranceDetails(insuranceList, vehicleDetails.getVehicleId());					
				}
				result = getVehicleDetailsDTO(vehicleDetails);
			}
		} catch (Exception e) {
			LoggerStackTraceUtil.printErrorMessage(e);
			throw e;
		}
		return result;
	}

	/**
	 * 
	 * @param vehicleId
	 * @throws BadRequestAlertException
	 */
	public void deleteVehicle(Long vehicleId) throws BadRequestAlertException {
		try {
			VehicleDetails vehicle = vehDetailsRepo.findByVehicleId(vehicleId);
			if (vehicle != null) {
				vehDetailsRepo.deleteByVehicleId(vehicleId);
			} else {
				throw new BadRequestAlertException("Vehicle with id: " + vehicleId + " does not exist");
			}

		} catch (Exception e) {
			LoggerStackTraceUtil.printErrorMessage(e);
			throw e;
		}
	}

	/**
	 * function to fetch vehicleDetailsDTO for orgs
	 * 
	 * @param orgIds
	 * @return
	 */
	public List<VehicleDetailsDTO> getVehicleListForOrgs(List<Long> orgIds) {
		List<VehicleDetailsDTO> vehicleList = new ArrayList<>();
		List<VehicleDetails> vehDetailsList = null;
		VehicleDetailsDTO dtoObj = null;
		vehDetailsList = vehDetailsRepo.findByOrgs(orgIds);
		for (VehicleDetails vehDetails : vehDetailsList) {
			dtoObj = new VehicleDetailsDTO();
			dtoObj = getVehicleDetailsDTO(vehDetails);
			vehicleList.add(dtoObj);
		}
		return vehicleList;
	}

	/**
	 * function to get VehicleDetailsDTO from VehicleDetails
	 * 
	 * @param vehDetails
	 * @return
	 */
	public VehicleDetailsDTO getVehicleDetailsDTO(VehicleDetails vehDetails) {

		VehicleDetailsDTO dtoObj = new VehicleDetailsDTO();

		Long deviceId = vehDetails.getDeviceId();
		Integer depotId = vehDetails.getDepotId();
		Integer vehicleSubtypeId = vehDetails.getVehicleSubtypeId();
		Long vehicleId = vehDetails.getVehicleId();
		Integer vehicleTypeId = vehDetails.getVehicleTypeId();
		dtoObj = mapper.map(vehDetails, VehicleDetailsDTO.class);

		if (deviceId != null) {
			Optional<DeviceDetails> device = deviceDetailsRepo.findById(deviceId);
			if (device.isPresent()) {
				dtoObj.setDeviceName(device.get().getDeviceName());
			}
		}
		List<Integer> tagIds = vehTagMappingRepo.findByVehicleId(vehicleId);
		if (!tagIds.isEmpty()) {
			List<Tags> tags = tagRepo.findAllById(tagIds);
			dtoObj.setTags(tags);
		}
		if (depotId != null) {
			Optional<DepotDetails> depot = depotDetailsRepo.findById(depotId);
			if (depot.isPresent()) {
				dtoObj.setDepotName(depot.get().getOrganization());
			}
		}
		if (vehicleTypeId != null) {
			Optional<VehicleType> vehicleType = vehicleTypeRepo.findById(vehicleTypeId);
			if (vehicleType.isPresent()) {
				dtoObj.setVehicleTypeName(vehicleType.get().getName());
			}
		}
		if (vehicleSubtypeId != null) {
			Optional<VehicleSubtype> vehicleSubtype = vehicleSubtypRepo.findById(vehicleSubtypeId);
			if (vehicleSubtype.isPresent()) {
				dtoObj.setVehicleSubtypeName(vehicleSubtype.get().getName());
			}
		}
		
		List<VehicleInsurance> insuranceList = insuranceRepo.findByVehicleId(vehicleId);
		List<InsuranceDTO> insurances= new ArrayList<>();
		InsuranceDTO insuranceObj;
		if(!insuranceList.isEmpty()) {
			for(VehicleInsurance insurance: insuranceList){
				insuranceObj = new InsuranceDTO();
				insuranceObj = mapper.map(insurance, InsuranceDTO.class);
				insurances.add(insuranceObj);
			}
			dtoObj.setInsurances(insurances);
		}

		return dtoObj;
	}
	/**
	 * function to save insurance details
	 * @param insuranceList
	 * @param vehicleId
	 */
	public void saveInsuranceDetails(List<InsuranceDTO> insuranceList,Long vehicleId){
		List<VehicleInsurance> vehInsuranceList = new ArrayList<>();
		VehicleInsurance insuranceObj;
		for(InsuranceDTO insurance : insuranceList) {
			insuranceObj =new VehicleInsurance();
			insuranceObj = mapper.map(insurance, VehicleInsurance.class);
			insuranceObj.setVehicleId(vehicleId);
			vehInsuranceList.add(insuranceObj);
		}
		insuranceRepo.saveAll(vehInsuranceList);
	}
}