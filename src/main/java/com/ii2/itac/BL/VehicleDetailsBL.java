package com.ii2.itac.BL;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ii2.itac.DTO.DeviceDetailsDTO;
import com.ii2.itac.entity.Department;
import com.ii2.itac.entity.DeviceDetails;
import com.ii2.itac.entity.VehicleSubtype;
import com.ii2.itac.repository.DepartmentRepository;
import com.ii2.itac.repository.DeviceDetailsRepository;
import com.ii2.itac.repository.OrgDepartmentMappingRepository;
import com.ii2.itac.repository.OrganizationRepository;
import com.ii2.itac.repository.UsersOrgsRepository;
import com.ii2.itac.repository.VehicleSubtypeRepository;
import com.ii2.itac.utilities.CommonUtils;

@Component
public class VehicleDetailsBL {	

	@Autowired
	VehicleSubtypeRepository vehicleSubtypeRepo;	

	@Autowired
	UsersOrgsRepository usersOrgsRepo;

	@Autowired
	OrganizationRepository orgRepo;

	@Autowired
	DeviceDetailsRepository deviceRepo;

	@Autowired
	DepartmentRepository departmentRepo;
	
	@Autowired
	CommonUtils commonUtils;
	
	@Autowired
	OrgDepartmentMappingRepository orgDeptRepo;

	

	/**
	 * 
	 * @param vehicleTypeId
	 * @return
	 */
	public List<VehicleSubtype> getVehicleSubtypes(Integer vehicleTypeId) {
		List<VehicleSubtype> vehicleSubtypes = null;
		vehicleSubtypes = vehicleSubtypeRepo.findByVehicleTypeId(vehicleTypeId);
		return vehicleSubtypes;
	}

	/**
	 * function to fetch devices of a user
	 * @param userId
	 * @return
	 */
	public List<DeviceDetailsDTO> getDevicesForUser(Long userId) {
		
		List<DeviceDetailsDTO> result = new ArrayList<>();
		DeviceDetailsDTO dtoObj = null;
		ModelMapper mapper = new ModelMapper();
		List<Long> activeOrgs = commonUtils.getActiveOrgsForUser(userId);
		if(!activeOrgs.isEmpty()) {
			List<DeviceDetails> deviceList = deviceRepo.findByOrgIds(activeOrgs);
			for (DeviceDetails device : deviceList) {
				dtoObj = new DeviceDetailsDTO();
				dtoObj = mapper.map(device, DeviceDetailsDTO.class);
				Integer departmentId = device.getDepartmentId();
				if(departmentId != null) {
					Optional<Department> dept = departmentRepo.findById(departmentId);
					if(dept.isPresent()) {
						dtoObj.setDepartMentName(dept.get().getDepartmentName());
					}
				}
				result.add(dtoObj);
			}
		}
		
		return result;
	}
	
	/**
	 * 
	 * @param userId
	 * @return
	 */
	public List<Department> getDepartments(Long userId) {
		List<Department> departments = null;
		List<Long> activeOrgs = commonUtils.getActiveOrgsForUser(userId);
		if(!activeOrgs.isEmpty()) {
			List<Integer> departmentIds = orgDeptRepo.findByOrgIds(activeOrgs);	
			if(!departmentIds.isEmpty()) {
				departments = departmentRepo.findAllById(departmentIds);
			}
		}
		return departments;
	}

}
