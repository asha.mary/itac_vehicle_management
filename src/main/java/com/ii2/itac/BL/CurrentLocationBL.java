package com.ii2.itac.BL;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ii2.itac.DTO.CurrentLocationDTO;
import com.ii2.itac.entity.DriverDetails;
import com.ii2.itac.entity.VehicleDetails;
import com.ii2.itac.entity.VehicleInfo;
import com.ii2.itac.repository.DepartmentRepository;
import com.ii2.itac.repository.DeviceDetailsRepository;
import com.ii2.itac.repository.DriverDetailsRepository;
import com.ii2.itac.repository.VehicleDetailsRepository;
import com.ii2.itac.repository.VehicleInfoRepository;
import com.ii2.itac.utilities.CommonUtils;
import com.itac.utilities.LoggerStackTraceUtil;

@Component
public class CurrentLocationBL {
	
	@Autowired 
	VehicleInfoRepository vehicleInfoRepo;
	
	@Autowired 
	VehicleDetailsRepository vehicleDetailsRepo;
	
	@Autowired 
	DriverDetailsRepository driverDetailsRepo;
	
	@Autowired 
	DepartmentRepository departmentRepo;
	
	@Autowired 
	DeviceDetailsRepository deviceRepo;
	
	public CurrentLocationDTO getCurrentLocation(Long vehicleId) {
		VehicleInfo vehicleInfo = new VehicleInfo();
		CurrentLocationDTO result = new CurrentLocationDTO();
		VehicleDetails vehicleDetails = new VehicleDetails();
		DriverDetails driverDetails = new DriverDetails();
		ModelMapper mapper = new ModelMapper();
		long timeInMilliseconds = 0;
		String deviceStatus = "";
		
		try {
			vehicleInfo = vehicleInfoRepo.findByVehicleId(vehicleId);
			vehicleDetails = vehicleDetailsRepo.findById(vehicleId).get();			
			result= mapper.map(vehicleDetails, CurrentLocationDTO.class);			
			result.setVehicleStatus(vehicleInfo.getVehicleStatus());
			result.setFuel(vehicleInfo.getStableFuelVal());
			result.setLattitude(vehicleInfo.getLattitude());
			result.setLongitude(vehicleInfo.getLongitude());
			result.setHeading(vehicleInfo.getHeading());
			timeInMilliseconds = vehicleInfo.getCreationTime().getTime();
			deviceStatus = new CommonUtils().getVehicleStatus(timeInMilliseconds);
			result.setDeviceStatus(deviceStatus);
			
		    if(vehicleDetails.getDeviceId() != null) {
		    	Long deviceId = vehicleDetails.getDeviceId();
				result.setDeviceName(deviceRepo.findById(deviceId).get().getDeviceName());	
		    	driverDetails = driverDetailsRepo.findByDeviceId(deviceId);
		    	result.setDriverName(driverDetails.getDriverName());
				result.setDepartment(departmentRepo.findById(driverDetails.getDepartmentId()).get().getDepartmentName());
				result.setPhoneNumber(driverDetails.getPhoneNumber());
		    }
		}catch(Exception e) {
			LoggerStackTraceUtil.printErrorMessage(e);
		}
		return result;
	}

}
