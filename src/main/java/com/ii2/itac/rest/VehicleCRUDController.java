package com.ii2.itac.rest;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ii2.itac.DTO.VehicleDetailsDTO;
import com.ii2.itac.exceptions.BadRequestAlertException;
import com.ii2.itac.exceptions.DataNotFoundException;
import com.ii2.itac.exceptions.NoActiveOrganizationException;
import com.ii2.itac.service.VehicleCrudService;

@RestController
@RequestMapping("/api")
@Transactional
public class VehicleCRUDController {

	@Autowired
	VehicleCrudService vehicleCrudService;

	/**
	 * 
	 * @param userId
	 * @return
	 * @throws NoActiveOrganizationException
	 */
	@GetMapping("/list-vehicles/{userId}")
	public ResponseEntity<List<VehicleDetailsDTO>> getVehicleListForUser(@PathVariable Long userId)
			throws NoActiveOrganizationException, DataNotFoundException {
		return new ResponseEntity<List<VehicleDetailsDTO>>(vehicleCrudService.getVehicleListForUser(userId),
				HttpStatus.OK);
	}

	/**
	 * 
	 * @param vehicle
	 * @return
	 * @throws BadRequestAlertException
	 */
	@PostMapping("/create-vehicle")
	public ResponseEntity<VehicleDetailsDTO> createVehicle(@Valid @RequestBody VehicleDetailsDTO vehicle)
			throws BadRequestAlertException {
		return new ResponseEntity<VehicleDetailsDTO>(vehicleCrudService.createVehicle(vehicle), HttpStatus.OK);
	}

	/**
	 * 
	 * @param vehicle
	 * @return
	 * @throws BadRequestAlertException
	 */
	@PutMapping("/update-vehicle")
	public ResponseEntity<VehicleDetailsDTO> updateVehicle(@Valid @RequestBody VehicleDetailsDTO vehicle)
			throws BadRequestAlertException {
		return new ResponseEntity<VehicleDetailsDTO>(vehicleCrudService.updateVehicle(vehicle), HttpStatus.OK);
	}

	/**
	 * 
	 * @param vehicleId
	 * @return
	 */
	@DeleteMapping("/delete-vehicle/{vehicleId}")
	public HttpStatus deleteVehicle(@PathVariable Long vehicleId) throws BadRequestAlertException{
		vehicleCrudService.deleteVehicle(vehicleId);
		return HttpStatus.OK;
	}

}
