package com.ii2.itac.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ii2.itac.DTO.CurrentLocationDTO;
import com.ii2.itac.service.CurrentLocationService;

@RestController
@RequestMapping("/api")
@Transactional
public class CurrentLocationController {
	
	@Autowired
	CurrentLocationService currentLocationService;
	
	@GetMapping("/current-location/{vehicleId}")
	public ResponseEntity<CurrentLocationDTO> getCurrentLocation(@PathVariable Long vehicleId){
		return new ResponseEntity<CurrentLocationDTO>(currentLocationService.getCurrentLocation(vehicleId),
				HttpStatus.OK);
	}

}
