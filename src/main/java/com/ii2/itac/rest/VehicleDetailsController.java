package com.ii2.itac.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ii2.itac.DTO.DeviceDetailsDTO;
import com.ii2.itac.entity.Department;
import com.ii2.itac.entity.Tags;
import com.ii2.itac.entity.VehicleSubtype;
import com.ii2.itac.entity.VehicleType;
import com.ii2.itac.service.VehicleDetailsService;

@RestController
@RequestMapping("/api")
@Transactional
public class VehicleDetailsController {
	
	
	@Autowired
	VehicleDetailsService vehicleDetailsService;
	
	/**
	 * 
	 * @return
	 */
	@GetMapping("/vehicle-types")
	public ResponseEntity<List<VehicleType>> getVehicleTypes(){
		return new ResponseEntity<List<VehicleType>>(vehicleDetailsService.getVehicleTypes(),
				HttpStatus.OK);
	}
	
	/**
	 * 
	 * @param vehicleTypeId
	 * @return
	 */
	@GetMapping("/vehicle-subtypes/{vehicleTypeId}")
	public ResponseEntity<List<VehicleSubtype>> getVehicleSubtypes(@PathVariable Integer vehicleTypeId){
		return new ResponseEntity<List<VehicleSubtype>>(vehicleDetailsService.getVehicleSubtypes(vehicleTypeId),
				HttpStatus.OK);
	}
	
	/**
	 * 
	 * @param userId
	 * @return
	 */
	@GetMapping("/tags")
	public ResponseEntity<List<Tags>> getTags(){
		return new ResponseEntity<List<Tags>>(vehicleDetailsService.getTags(),
				HttpStatus.OK);
	}
	
	/**
	 * 
	 * @param userId
	 * @return
	 */
	@GetMapping("/devices/{userId}")
	public ResponseEntity<List<DeviceDetailsDTO>> getDevicesForUser(@PathVariable Long userId){
		return new ResponseEntity<List<DeviceDetailsDTO>>(vehicleDetailsService.getDevicesForUser(userId),
				HttpStatus.OK);
	}
	
	/**
	 * 
	 * @param userId
	 * @return
	 */
	@GetMapping("/departments/{userId}")
	public ResponseEntity<List<Department>> getDepartments(@PathVariable Long userId){
		return new ResponseEntity<List<Department>>(vehicleDetailsService.getDepartments(userId),
				HttpStatus.OK);
	}
	
}
