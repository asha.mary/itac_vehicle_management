package com.ii2.itac.helper;


import java.io.InputStream;
import java.net.URL;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;

@Component
public class S3Manager {
	//test key
	
	@Value("${app.s3.accessKey}")
	String ACCESSKEY;

	@Value("${app.s3.secretKey}")
	String SECRETKEY;

		
		private AmazonS3 getS3Client() {
			// to be read from config server
			AWSCredentials credentials = new BasicAWSCredentials(ACCESSKEY, SECRETKEY);
			
			return AmazonS3ClientBuilder.standard().withCredentials(new AWSStaticCredentialsProvider(credentials))
					.withRegion(Regions.AP_SOUTH_1).build();
		}
		
		/**
		 * Function to upload file to S3
		 * @param bucketName
		 * @param fileName
		 * @param fileContent
		 * @return
		 */
		public String uploadFile(String bucketName, String fileName, InputStream fileContent) {

			try {
				AmazonS3 s3client = getS3Client();
				
				ObjectMetadata metadata = new ObjectMetadata();
	            metadata.setContentType("plain/text");
	            metadata.addUserMetadata("title", fileName);

				s3client.putObject(new PutObjectRequest(bucketName, fileName, fileContent, metadata).withCannedAcl(CannedAccessControlList.PublicRead));

				URL s3Url = s3client.getUrl(bucketName, fileName);

				return s3Url.toExternalForm();
			} catch (Exception exp) {
				exp.printStackTrace();
			}

			return null;
		}

		/**
		 * Function to upload json to S3
		 * 
		 * @param bucketName
		 * @param fileName
		 * @param json
		 * @return
		 */
		public String uploadJSON(String bucketName, String fileName, String json) {

			try {
				AmazonS3 s3client = getS3Client();

				s3client.putObject(bucketName, fileName, json);

				URL s3Url = s3client.getUrl(bucketName, fileName);

				return s3Url.toExternalForm();
			} catch (Exception exp) {
				exp.printStackTrace();
			}

			return null;
		}	

}
